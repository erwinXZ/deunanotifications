const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.pushNotification = functions.firestore.document('deliveries/{deliveryId}').onUpdate((change, context) => {

    console.log('Push notification event triggered');

    const newValue = change.after.data();

    // access a particular field as you would any JS property
    let colUsersRef = admin.firestore().collection('users');
    const status = newValue.status.toString();
    const notified = newValue.notified;
    const deliveryId = newValue.id.toString();
    const driverId = newValue.driverId;
    const fullNameDriver = newValue.fullNameDriver;
    const imageUrl = newValue.imageUrl;
    const clientToken = newValue.clientToken;
    const correlativeNumber = newValue.correlativeNumber;
    const businessesIds = newValue.businessesIds;
    const businessesIdsConfirmed = newValue.businessesIdsConfirmed;
    const businessesIdsCollected = newValue.businessesIdsCollected;
    const businessToken = newValue.businessToken;
    let userTokens = [];
    let subject = "";
    let body = {};

    //CANCELLED
    if(status == 5) {
        body = {
            "text":"Lo sentimos, su pedido " + "#" + correlativeNumber + " fué cancelado",
            "deliveryId": deliveryId,
            "status": status
        }
        const payload = {
          "notification": {
              "title": "Pedido Cancelado",
              "body": body.text,
              "sound": "notification_sound",
              "android_channel_id": "channel_01",
              "android_driver_channel_id": "channel_driver",
              "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
              "click_action": "OPEN_ACTIVITY_CANCEL"
          },
          "data": body
        };
        userTokens.push(clientToken);
        if (driverId != null) {
          return colUsersRef.doc(driverId.toString()).get()
          .then(driver => {
            if (driver.exists) {
              if (driver.get('userToken') != null) {
                userTokens.push(driver.get('userToken'));
              }
              console.log('NOTIFICACION DE CANCELADO A CLIENTE Y CONDUCTOR, userTokens: ', userTokens);
              admin.messaging().sendToDevice(userTokens, payload);
            }
          })
        } else {
          console.log('NOTIFICACION DE CANCELADO A CLIENTE, userTokens: ', userTokens);
          return admin.messaging().sendToDevice(userTokens, payload);
        }
    }
    //ASIGNED
    if(status == 2) {
      body = {
          "text": "Tienes un nuevo pedido " + "#" + correlativeNumber,
          "deliveryId": deliveryId,
          "status": status
      };

      const payload = {
        "notification": {
            "title": "Nuevo Pedido",
            "body": body.text,
            "sound": "notification_sound",
            "android_driver_channel_id": "channel_driver",
            "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012"
        },
        "data": body
      };

      return colUsersRef.doc(driverId.toString()).get()
      .then(driver => {
        if (driver.exists) {
          if (driver.get('userToken') != null) {
            userTokens.push(driver.get('userToken'));
          }
          console.log('NOTIFICACION DE ASIGNADO A CONDUCTOR, userTokens: ', userTokens);
          admin.messaging().sendToDevice(userTokens, payload);
        }
      })
  }

  //DELIVERED
  if(status == 4) {
    subject = "Pedido Finalizado";
    body = {
        "text": "Su pedido" + "#" + correlativeNumber + " fué entregado exitosamente",
        "deliveryId": deliveryId,
        "driverId": driverId.toString(),
        "fullNameDriver": fullNameDriver.toString(),
        "imageUrl": imageUrl == null ? '' : imageUrl.toString(),
        "status": status
    }
    // Create a notification
    const payload = {
      "notification": {
          "title": subject,
          "body": body.text,
          "sound": "notification_sound",
          "android_channel_id": "channel_01",
          "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
          "click_action": "OPEN_ACTIVITY_DELIVERED"
      },
      "data": body
    };
    console.log('NOTIFICACION DE PEDIDO FINALIZADO, clientToken: ', clientToken);
    return admin.messaging().sendToDevice(clientToken, payload);
  }

  //CONFIRMED
  if(status == 3) {
    // Para notificar varias veces
    if(notified==true) {
      return colUsersRef.doc(driverId.toString()).get()
        .then(driver => {
          if (driver.exists) {
            body = {
              "text": driver.get('fullName') + " le está esperando con su pedido",
              "deliveryId": deliveryId,
              "status": status
            }
            const payload = {
              "notification": {
                  "title": "Su pedido ha llegado!!!",
                  "body": body.text,
                  "sound": "notification_sound",
                  "android_channel_id": "channel_01",
                  "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012"
              },
              "data": body
            };
            userTokens.push(clientToken);
            console.log('NOTIFICACION TIMBRE, userTokens: ', userTokens);
            admin.messaging().sendToDevice(userTokens, payload);
          }
        })
    }
    if (notified == false) {
      return 0;
    }
    // Para las notificaciones de productos recogidos
    if (businessesIdsCollected != null && businessesIdsCollected.length != 0) {
      businessId = businessesIdsCollected[businessesIdsCollected.length - 1];
      return colUsersRef.where('businessId', '==', businessId).limit(1).get().then(businessesUsers => {
        businessesUsers.forEach(businessUser => {
          body = {
            "text": fullNameDriver + " recogió los productos de " + businessUser.get('businessName') + " de su pedido #" + correlativeNumber,
            "deliveryId": deliveryId,
            "status": status
          };
          const payload = {
            "notification": {
                "title": "Productos recogidos",
                "body": body.text,
                "sound": "notification_sound",
                "android_channel_id": "channel_01",
                "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
                "click_action": "OPEN_ACTIVITY_ON_WAY"
            },
            "data": body
          };
          userTokens.push(clientToken);
          console.log('NOTIFICACION PRODUCTOS RECOGIDOS DE ' + businessUser.get('businessName') +  ', userTokens: ', userTokens);
          admin.messaging().sendToDevice(userTokens, payload);
        })
        
      })
    }

    // Para notificar una vez que el pedido está en camino
    if (notified == null) {
      subject = clientToken ? "Pedido en Camino" : "Su pedido está en camino";
      body = {
          "text": clientToken ? fullNameDriver + " está esperando tu pedido #" + correlativeNumber + " en el Negocio. ¡Puedes seguir a la motito!" :
            "Su pedido de moto fué confirmado y está en camino",
          "deliveryId": deliveryId,
          "status": status
      };
      const payload = {
        "notification": {
            "title": subject,
            "body": body.text,
            "sound": "notification_sound",
            "android_channel_id": "channel_01",
            "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
            "click_action": "OPEN_ACTIVITY_ON_WAY"
        },
        "data": body
      };
      
      console.log('NOTIFICACION DE PEDIDO EN CAMINO, clientToken: ', clientToken ? clientToken : businessToken);
      return admin.messaging().sendToDevice(clientToken ? clientToken : businessToken, payload);
    }
  }

  if (status == 1) {
    if (businessesIdsConfirmed != null && businessesIdsConfirmed.length != 0) {
      businessId = businessesIdsConfirmed[businessesIdsConfirmed.length - 1];
      return colUsersRef.where('businessId', '==', businessId).limit(1).get().then(businessesUsers => {
        businessesUsers.forEach(businessUser => {
          colUsersRef.where('roles', 'array-contains', "ROLE_USER").get()
          .then(administrators => {
            administrators.forEach(administrator => {
              if (administrator.get('tokens') != null) {
                administrator.get('tokens').forEach(token => {
                  userTokens.push(token);
                });
              }
            });
            body = {
              "text": businessUser.get('businessName') + " está procesando su pedido #" + correlativeNumber,
              "deliveryId": deliveryId,
              "status": status
            };
            const payload = {
              notification: {
                "title": "Confirmación de Negocio",
                "body": body.text,
                "sound": "notification_sound",
                "android_channel_id": "channel_01",
                "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
                "click_action": "OPEN_ORDER_HISTORY"
              },
              "data": body
            };
            userTokens.push(clientToken);
            console.log('NOTIFICACION DE CONFIRMACIÓN DE NEGOCIO ' + businessUser.get('businessName') + ', userTokens: ', userTokens);
            admin.messaging().sendToDevice(userTokens, payload);
          });
        })
        // if (businessesIds.length == businessesIdsConfirmed.length) {
        //   body = {
        //     "text":"Su Pedido fué confirmado",
        //     "deliveryId": deliveryId,
        //     "status": status,
        //     "confirmed": "true"
        //   }
        //   const payload = {
        //     "notification": {
        //         "title": "Pedido Confirmado",
        //         "body": body.text,
        //         "sound": "notification_sound",
        //         "android_channel_id": "channel_01",
        //         "icon": "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012",
        //         "click_action": "OPEN_MAIN"
        //     },
        //     "data": body
        //   };
        //   console.log('NOTIFICACION DE CONFIRMACIÓN DE TODOS LOS NEGOCIOS, userTokens: ', userTokens);
        //   admin.messaging().sendToDevice([clientToken], payload);
        // }
      })
    }
  }
});


exports.pushNotificationAdmin = functions.firestore.document('deliveries/{deliveryId}').onCreate((snapshot, context) => {
    const newValue = snapshot.data();
    const businessesIds = newValue.businessesIds;
    let userTokens = new Set();
    let colUsersRef = admin.firestore().collection('users');

    return colUsersRef.where('roles', 'array-contains', "ROLE_USER").get()
    .then(administrators => {
      administrators.forEach(administrator => {
        if (administrator.get('tokens') != null) {
          administrator.get('tokens').forEach(token => {
            userTokens.add(token);
          });
        }
      });
      businessesIds.forEach(businessId => {
        colUsersRef.where('businessId', '==', businessId).get().then(businessesUsers => {
          businessesUsers.forEach(user => {
            if (user.get('tokens') != null) {
              user.get('tokens').forEach(token => {
                userTokens.add(token);
              });
            }
          });
          const payload = {
            notification: {
              title: "Pedido Pendiente",
              body: "Tiene un nuevo pedido",
              sound: "content/sound/notification_sound.mp3",
              icon: "https://firebasestorage.googleapis.com/v0/b/deuna-5a924.appspot.com/o/web_hi_res_512.png?alt=media&token=2003ab9f-3607-4bce-a118-233ab21e9012"
            }
          };
          console.log('NOTIFICACION ADMIN Y NEGOCIOS DE PEDIDO PENDIENTE, userTokens: ', Array.from(userTokens));
          admin.messaging().sendToDevice([...userTokens], payload);
        });
      });
    })
    .catch(err => {
      console.log('Error getting documents', err);
    });
});
